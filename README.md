# Progetto_LAB_IA_GI_2021_22
Progetto Laboratorio di Intelligenza Artificiale e Grafica Interattiva A.A. 2021/22
Progetto: Pick and Delivery
Autore: Flavio Mencobello, matricola 1871843
Il progetto sviluppato implementa un programma di "pick and delivery" per un robot trasportatore situato all'interno di un qualsiasi luogo di cui la mappa data come input è rappresentativa. Per la funzione del programma, sono utilizzati elementi della repository labiagi_2020_21 del corso di laboratorio. Tali elementi sono: map server, localizer, planner e path follower. Per verificare, perciò, le funzionalità del progetto, devono essere prima avviati i vari supplementi elencati. Primo su tutti, va attivato il launcher_stage, il quale apre il programma stageros con in lettura la mappa diag.world interna al repository del progetto. Attivate tutte le componenti, si può avviare il pad_server, la componente legata al robot trasportatore. Infine, si possono attivare i pad_client che si collegheranno al robot per inviargli le richieste di pick and delivery: per semplificare la dimostrazione, si possono avviare i nodi launcher1 e launcher2, rappresentati due differenti client che si collegano al robot.
Ogni client, al collegamento, passa al nodo (nei launcher il passaggio è automatico) una serie di file di testo: ognuno rappresenta una stanza disponibile all'interno della mappa utilizzata, con coordinate, nome e codice identificativo. Per utilizzare il programma, il client deve accedere tramite nome e codice della stanza da cui agisce, così da essere riconosciuto dal robot. Avvenuto il login, viene presentata la lista dei comandi disponibili: 
-call (chiama il robot nella stanza)
-delete (annulla l'ultimo comando/libera il robot chiamato)
-quit (disconnette il client dal server)
-room_name (comando per inserire la stanza destinataria)
-commands (ripropone la lista dei comandi)
