;; Auto-generated. Do not edit!


(when (boundp 'pick_and_delivery::RoomAddressee)
  (if (not (find-package "PICK_AND_DELIVERY"))
    (make-package "PICK_AND_DELIVERY"))
  (shadow 'RoomAddressee (find-package "PICK_AND_DELIVERY")))
(unless (find-package "PICK_AND_DELIVERY::ROOMADDRESSEE")
  (make-package "PICK_AND_DELIVERY::ROOMADDRESSEE"))
(unless (find-package "PICK_AND_DELIVERY::ROOMADDRESSEEREQUEST")
  (make-package "PICK_AND_DELIVERY::ROOMADDRESSEEREQUEST"))
(unless (find-package "PICK_AND_DELIVERY::ROOMADDRESSEERESPONSE")
  (make-package "PICK_AND_DELIVERY::ROOMADDRESSEERESPONSE"))

(in-package "ROS")





(defclass pick_and_delivery::RoomAddresseeRequest
  :super ros::object
  :slots (_x _y _theta _room_addr _room_send _room_command ))

(defmethod pick_and_delivery::RoomAddresseeRequest
  (:init
   (&key
    ((:x __x) 0.0)
    ((:y __y) 0.0)
    ((:theta __theta) 0.0)
    ((:room_addr __room_addr) "")
    ((:room_send __room_send) "")
    ((:room_command __room_command) "")
    )
   (send-super :init)
   (setq _x (float __x))
   (setq _y (float __y))
   (setq _theta (float __theta))
   (setq _room_addr (string __room_addr))
   (setq _room_send (string __room_send))
   (setq _room_command (string __room_command))
   self)
  (:x
   (&optional __x)
   (if __x (setq _x __x)) _x)
  (:y
   (&optional __y)
   (if __y (setq _y __y)) _y)
  (:theta
   (&optional __theta)
   (if __theta (setq _theta __theta)) _theta)
  (:room_addr
   (&optional __room_addr)
   (if __room_addr (setq _room_addr __room_addr)) _room_addr)
  (:room_send
   (&optional __room_send)
   (if __room_send (setq _room_send __room_send)) _room_send)
  (:room_command
   (&optional __room_command)
   (if __room_command (setq _room_command __room_command)) _room_command)
  (:serialization-length
   ()
   (+
    ;; float32 _x
    4
    ;; float32 _y
    4
    ;; float32 _theta
    4
    ;; string _room_addr
    4 (length _room_addr)
    ;; string _room_send
    4 (length _room_send)
    ;; string _room_command
    4 (length _room_command)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float32 _x
       (sys::poke _x (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _y
       (sys::poke _y (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; float32 _theta
       (sys::poke _theta (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; string _room_addr
       (write-long (length _room_addr) s) (princ _room_addr s)
     ;; string _room_send
       (write-long (length _room_send) s) (princ _room_send s)
     ;; string _room_command
       (write-long (length _room_command) s) (princ _room_command s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float32 _x
     (setq _x (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _y
     (setq _y (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; float32 _theta
     (setq _theta (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; string _room_addr
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _room_addr (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _room_send
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _room_send (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _room_command
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _room_command (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass pick_and_delivery::RoomAddresseeResponse
  :super ros::object
  :slots (_response ))

(defmethod pick_and_delivery::RoomAddresseeResponse
  (:init
   (&key
    ((:response __response) "")
    )
   (send-super :init)
   (setq _response (string __response))
   self)
  (:response
   (&optional __response)
   (if __response (setq _response __response)) _response)
  (:serialization-length
   ()
   (+
    ;; string _response
    4 (length _response)
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _response
       (write-long (length _response) s) (princ _response s)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _response
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _response (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;;
   self)
  )

(defclass pick_and_delivery::RoomAddressee
  :super ros::object
  :slots ())

(setf (get pick_and_delivery::RoomAddressee :md5sum-) "a5d6422f6567a1a1a33df37ff1866e83")
(setf (get pick_and_delivery::RoomAddressee :datatype-) "pick_and_delivery/RoomAddressee")
(setf (get pick_and_delivery::RoomAddressee :request) pick_and_delivery::RoomAddresseeRequest)
(setf (get pick_and_delivery::RoomAddressee :response) pick_and_delivery::RoomAddresseeResponse)

(defmethod pick_and_delivery::RoomAddresseeRequest
  (:response () (instance pick_and_delivery::RoomAddresseeResponse :init)))

(setf (get pick_and_delivery::RoomAddresseeRequest :md5sum-) "a5d6422f6567a1a1a33df37ff1866e83")
(setf (get pick_and_delivery::RoomAddresseeRequest :datatype-) "pick_and_delivery/RoomAddresseeRequest")
(setf (get pick_and_delivery::RoomAddresseeRequest :definition-)
      "float32 x
float32 y
float32 theta
string room_addr
string room_send
string room_command
---
string response

")

(setf (get pick_and_delivery::RoomAddresseeResponse :md5sum-) "a5d6422f6567a1a1a33df37ff1866e83")
(setf (get pick_and_delivery::RoomAddresseeResponse :datatype-) "pick_and_delivery/RoomAddresseeResponse")
(setf (get pick_and_delivery::RoomAddresseeResponse :definition-)
      "float32 x
float32 y
float32 theta
string room_addr
string room_send
string room_command
---
string response

")



(provide :pick_and_delivery/RoomAddressee "a5d6422f6567a1a1a33df37ff1866e83")


