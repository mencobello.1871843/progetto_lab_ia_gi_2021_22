;; Auto-generated. Do not edit!


(when (boundp 'pick_and_delivery::Comunication)
  (if (not (find-package "PICK_AND_DELIVERY"))
    (make-package "PICK_AND_DELIVERY"))
  (shadow 'Comunication (find-package "PICK_AND_DELIVERY")))
(unless (find-package "PICK_AND_DELIVERY::COMUNICATION")
  (make-package "PICK_AND_DELIVERY::COMUNICATION"))

(in-package "ROS")
;;//! \htmlinclude Comunication.msg.html


(defclass pick_and_delivery::Comunication
  :super ros::object
  :slots (_sender _addressee _taken _arrived _failed ))

(defmethod pick_and_delivery::Comunication
  (:init
   (&key
    ((:sender __sender) "")
    ((:addressee __addressee) "")
    ((:taken __taken) nil)
    ((:arrived __arrived) nil)
    ((:failed __failed) nil)
    )
   (send-super :init)
   (setq _sender (string __sender))
   (setq _addressee (string __addressee))
   (setq _taken __taken)
   (setq _arrived __arrived)
   (setq _failed __failed)
   self)
  (:sender
   (&optional __sender)
   (if __sender (setq _sender __sender)) _sender)
  (:addressee
   (&optional __addressee)
   (if __addressee (setq _addressee __addressee)) _addressee)
  (:taken
   (&optional __taken)
   (if __taken (setq _taken __taken)) _taken)
  (:arrived
   (&optional __arrived)
   (if __arrived (setq _arrived __arrived)) _arrived)
  (:failed
   (&optional __failed)
   (if __failed (setq _failed __failed)) _failed)
  (:serialization-length
   ()
   (+
    ;; string _sender
    4 (length _sender)
    ;; string _addressee
    4 (length _addressee)
    ;; bool _taken
    1
    ;; bool _arrived
    1
    ;; bool _failed
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _sender
       (write-long (length _sender) s) (princ _sender s)
     ;; string _addressee
       (write-long (length _addressee) s) (princ _addressee s)
     ;; bool _taken
       (if _taken (write-byte -1 s) (write-byte 0 s))
     ;; bool _arrived
       (if _arrived (write-byte -1 s) (write-byte 0 s))
     ;; bool _failed
       (if _failed (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _sender
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _sender (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; string _addressee
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _addressee (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; bool _taken
     (setq _taken (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _arrived
     (setq _arrived (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;; bool _failed
     (setq _failed (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get pick_and_delivery::Comunication :md5sum-) "188732a570db7a36788ed44270995502")
(setf (get pick_and_delivery::Comunication :datatype-) "pick_and_delivery/Comunication")
(setf (get pick_and_delivery::Comunication :definition-)
      "string sender
string addressee
bool taken
bool arrived
bool failed

")



(provide :pick_and_delivery/Comunication "188732a570db7a36788ed44270995502")


