;; Auto-generated. Do not edit!


(when (boundp 'pick_and_delivery::OccupiedBool)
  (if (not (find-package "PICK_AND_DELIVERY"))
    (make-package "PICK_AND_DELIVERY"))
  (shadow 'OccupiedBool (find-package "PICK_AND_DELIVERY")))
(unless (find-package "PICK_AND_DELIVERY::OCCUPIEDBOOL")
  (make-package "PICK_AND_DELIVERY::OCCUPIEDBOOL"))

(in-package "ROS")
;;//! \htmlinclude OccupiedBool.msg.html


(defclass pick_and_delivery::OccupiedBool
  :super ros::object
  :slots (_occupied ))

(defmethod pick_and_delivery::OccupiedBool
  (:init
   (&key
    ((:occupied __occupied) nil)
    )
   (send-super :init)
   (setq _occupied __occupied)
   self)
  (:occupied
   (&optional __occupied)
   (if __occupied (setq _occupied __occupied)) _occupied)
  (:serialization-length
   ()
   (+
    ;; bool _occupied
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; bool _occupied
       (if _occupied (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; bool _occupied
     (setq _occupied (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get pick_and_delivery::OccupiedBool :md5sum-) "44d7026c7dcc8daf286cc35a49a8f442")
(setf (get pick_and_delivery::OccupiedBool :datatype-) "pick_and_delivery/OccupiedBool")
(setf (get pick_and_delivery::OccupiedBool :definition-)
      "bool occupied

")



(provide :pick_and_delivery/OccupiedBool "44d7026c7dcc8daf286cc35a49a8f442")


