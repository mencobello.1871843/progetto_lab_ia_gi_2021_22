; Auto-generated. Do not edit!


(cl:in-package pick_and_delivery-msg)


;//! \htmlinclude LoginName.msg.html

(cl:defclass <LoginName> (roslisp-msg-protocol:ros-message)
  ((name
    :reader name
    :initarg :name
    :type cl:string
    :initform "")
   (log
    :reader log
    :initarg :log
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass LoginName (<LoginName>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <LoginName>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'LoginName)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name pick_and_delivery-msg:<LoginName> is deprecated: use pick_and_delivery-msg:LoginName instead.")))

(cl:ensure-generic-function 'name-val :lambda-list '(m))
(cl:defmethod name-val ((m <LoginName>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pick_and_delivery-msg:name-val is deprecated.  Use pick_and_delivery-msg:name instead.")
  (name m))

(cl:ensure-generic-function 'log-val :lambda-list '(m))
(cl:defmethod log-val ((m <LoginName>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pick_and_delivery-msg:log-val is deprecated.  Use pick_and_delivery-msg:log instead.")
  (log m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <LoginName>) ostream)
  "Serializes a message object of type '<LoginName>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'name))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'log) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <LoginName>) istream)
  "Deserializes a message object of type '<LoginName>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:setf (cl:slot-value msg 'log) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<LoginName>)))
  "Returns string type for a message object of type '<LoginName>"
  "pick_and_delivery/LoginName")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'LoginName)))
  "Returns string type for a message object of type 'LoginName"
  "pick_and_delivery/LoginName")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<LoginName>)))
  "Returns md5sum for a message object of type '<LoginName>"
  "6949c869b9f974ee2e8f6741c95510d2")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'LoginName)))
  "Returns md5sum for a message object of type 'LoginName"
  "6949c869b9f974ee2e8f6741c95510d2")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<LoginName>)))
  "Returns full string definition for message of type '<LoginName>"
  (cl:format cl:nil "string name~%bool log~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'LoginName)))
  "Returns full string definition for message of type 'LoginName"
  (cl:format cl:nil "string name~%bool log~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <LoginName>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'name))
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <LoginName>))
  "Converts a ROS message object to a list"
  (cl:list 'LoginName
    (cl:cons ':name (name msg))
    (cl:cons ':log (log msg))
))
