; Auto-generated. Do not edit!


(cl:in-package pick_and_delivery-msg)


;//! \htmlinclude OccupiedBool.msg.html

(cl:defclass <OccupiedBool> (roslisp-msg-protocol:ros-message)
  ((occupied
    :reader occupied
    :initarg :occupied
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass OccupiedBool (<OccupiedBool>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <OccupiedBool>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'OccupiedBool)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name pick_and_delivery-msg:<OccupiedBool> is deprecated: use pick_and_delivery-msg:OccupiedBool instead.")))

(cl:ensure-generic-function 'occupied-val :lambda-list '(m))
(cl:defmethod occupied-val ((m <OccupiedBool>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader pick_and_delivery-msg:occupied-val is deprecated.  Use pick_and_delivery-msg:occupied instead.")
  (occupied m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <OccupiedBool>) ostream)
  "Serializes a message object of type '<OccupiedBool>"
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'occupied) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <OccupiedBool>) istream)
  "Deserializes a message object of type '<OccupiedBool>"
    (cl:setf (cl:slot-value msg 'occupied) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<OccupiedBool>)))
  "Returns string type for a message object of type '<OccupiedBool>"
  "pick_and_delivery/OccupiedBool")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'OccupiedBool)))
  "Returns string type for a message object of type 'OccupiedBool"
  "pick_and_delivery/OccupiedBool")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<OccupiedBool>)))
  "Returns md5sum for a message object of type '<OccupiedBool>"
  "44d7026c7dcc8daf286cc35a49a8f442")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'OccupiedBool)))
  "Returns md5sum for a message object of type 'OccupiedBool"
  "44d7026c7dcc8daf286cc35a49a8f442")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<OccupiedBool>)))
  "Returns full string definition for message of type '<OccupiedBool>"
  (cl:format cl:nil "bool occupied~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'OccupiedBool)))
  "Returns full string definition for message of type 'OccupiedBool"
  (cl:format cl:nil "bool occupied~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <OccupiedBool>))
  (cl:+ 0
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <OccupiedBool>))
  "Converts a ROS message object to a list"
  (cl:list 'OccupiedBool
    (cl:cons ':occupied (occupied msg))
))
