// Auto-generated. Do not edit!

// (in-package pick_and_delivery.srv)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------


//-----------------------------------------------------------

class RoomAddresseeRequest {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.x = null;
      this.y = null;
      this.theta = null;
      this.room_addr = null;
      this.room_send = null;
      this.room_command = null;
    }
    else {
      if (initObj.hasOwnProperty('x')) {
        this.x = initObj.x
      }
      else {
        this.x = 0.0;
      }
      if (initObj.hasOwnProperty('y')) {
        this.y = initObj.y
      }
      else {
        this.y = 0.0;
      }
      if (initObj.hasOwnProperty('theta')) {
        this.theta = initObj.theta
      }
      else {
        this.theta = 0.0;
      }
      if (initObj.hasOwnProperty('room_addr')) {
        this.room_addr = initObj.room_addr
      }
      else {
        this.room_addr = '';
      }
      if (initObj.hasOwnProperty('room_send')) {
        this.room_send = initObj.room_send
      }
      else {
        this.room_send = '';
      }
      if (initObj.hasOwnProperty('room_command')) {
        this.room_command = initObj.room_command
      }
      else {
        this.room_command = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RoomAddresseeRequest
    // Serialize message field [x]
    bufferOffset = _serializer.float32(obj.x, buffer, bufferOffset);
    // Serialize message field [y]
    bufferOffset = _serializer.float32(obj.y, buffer, bufferOffset);
    // Serialize message field [theta]
    bufferOffset = _serializer.float32(obj.theta, buffer, bufferOffset);
    // Serialize message field [room_addr]
    bufferOffset = _serializer.string(obj.room_addr, buffer, bufferOffset);
    // Serialize message field [room_send]
    bufferOffset = _serializer.string(obj.room_send, buffer, bufferOffset);
    // Serialize message field [room_command]
    bufferOffset = _serializer.string(obj.room_command, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RoomAddresseeRequest
    let len;
    let data = new RoomAddresseeRequest(null);
    // Deserialize message field [x]
    data.x = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [y]
    data.y = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [theta]
    data.theta = _deserializer.float32(buffer, bufferOffset);
    // Deserialize message field [room_addr]
    data.room_addr = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [room_send]
    data.room_send = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [room_command]
    data.room_command = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.room_addr.length;
    length += object.room_send.length;
    length += object.room_command.length;
    return length + 24;
  }

  static datatype() {
    // Returns string type for a service object
    return 'pick_and_delivery/RoomAddresseeRequest';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '99ae33bb678302a4628d493d8ed98651';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    float32 x
    float32 y
    float32 theta
    string room_addr
    string room_send
    string room_command
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RoomAddresseeRequest(null);
    if (msg.x !== undefined) {
      resolved.x = msg.x;
    }
    else {
      resolved.x = 0.0
    }

    if (msg.y !== undefined) {
      resolved.y = msg.y;
    }
    else {
      resolved.y = 0.0
    }

    if (msg.theta !== undefined) {
      resolved.theta = msg.theta;
    }
    else {
      resolved.theta = 0.0
    }

    if (msg.room_addr !== undefined) {
      resolved.room_addr = msg.room_addr;
    }
    else {
      resolved.room_addr = ''
    }

    if (msg.room_send !== undefined) {
      resolved.room_send = msg.room_send;
    }
    else {
      resolved.room_send = ''
    }

    if (msg.room_command !== undefined) {
      resolved.room_command = msg.room_command;
    }
    else {
      resolved.room_command = ''
    }

    return resolved;
    }
};

class RoomAddresseeResponse {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.response = null;
    }
    else {
      if (initObj.hasOwnProperty('response')) {
        this.response = initObj.response
      }
      else {
        this.response = '';
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type RoomAddresseeResponse
    // Serialize message field [response]
    bufferOffset = _serializer.string(obj.response, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type RoomAddresseeResponse
    let len;
    let data = new RoomAddresseeResponse(null);
    // Deserialize message field [response]
    data.response = _deserializer.string(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.response.length;
    return length + 4;
  }

  static datatype() {
    // Returns string type for a service object
    return 'pick_and_delivery/RoomAddresseeResponse';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '6de314e2dc76fbff2b6244a6ad70b68d';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string response
    
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new RoomAddresseeResponse(null);
    if (msg.response !== undefined) {
      resolved.response = msg.response;
    }
    else {
      resolved.response = ''
    }

    return resolved;
    }
};

module.exports = {
  Request: RoomAddresseeRequest,
  Response: RoomAddresseeResponse,
  md5sum() { return 'a5d6422f6567a1a1a33df37ff1866e83'; },
  datatype() { return 'pick_and_delivery/RoomAddressee'; }
};
