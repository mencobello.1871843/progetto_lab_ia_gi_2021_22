// Auto-generated. Do not edit!

// (in-package pick_and_delivery.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Comunication {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.sender = null;
      this.addressee = null;
      this.taken = null;
      this.arrived = null;
      this.failed = null;
    }
    else {
      if (initObj.hasOwnProperty('sender')) {
        this.sender = initObj.sender
      }
      else {
        this.sender = '';
      }
      if (initObj.hasOwnProperty('addressee')) {
        this.addressee = initObj.addressee
      }
      else {
        this.addressee = '';
      }
      if (initObj.hasOwnProperty('taken')) {
        this.taken = initObj.taken
      }
      else {
        this.taken = false;
      }
      if (initObj.hasOwnProperty('arrived')) {
        this.arrived = initObj.arrived
      }
      else {
        this.arrived = false;
      }
      if (initObj.hasOwnProperty('failed')) {
        this.failed = initObj.failed
      }
      else {
        this.failed = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Comunication
    // Serialize message field [sender]
    bufferOffset = _serializer.string(obj.sender, buffer, bufferOffset);
    // Serialize message field [addressee]
    bufferOffset = _serializer.string(obj.addressee, buffer, bufferOffset);
    // Serialize message field [taken]
    bufferOffset = _serializer.bool(obj.taken, buffer, bufferOffset);
    // Serialize message field [arrived]
    bufferOffset = _serializer.bool(obj.arrived, buffer, bufferOffset);
    // Serialize message field [failed]
    bufferOffset = _serializer.bool(obj.failed, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Comunication
    let len;
    let data = new Comunication(null);
    // Deserialize message field [sender]
    data.sender = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [addressee]
    data.addressee = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [taken]
    data.taken = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [arrived]
    data.arrived = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [failed]
    data.failed = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.sender.length;
    length += object.addressee.length;
    return length + 11;
  }

  static datatype() {
    // Returns string type for a message object
    return 'pick_and_delivery/Comunication';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '188732a570db7a36788ed44270995502';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string sender
    string addressee
    bool taken
    bool arrived
    bool failed
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Comunication(null);
    if (msg.sender !== undefined) {
      resolved.sender = msg.sender;
    }
    else {
      resolved.sender = ''
    }

    if (msg.addressee !== undefined) {
      resolved.addressee = msg.addressee;
    }
    else {
      resolved.addressee = ''
    }

    if (msg.taken !== undefined) {
      resolved.taken = msg.taken;
    }
    else {
      resolved.taken = false
    }

    if (msg.arrived !== undefined) {
      resolved.arrived = msg.arrived;
    }
    else {
      resolved.arrived = false
    }

    if (msg.failed !== undefined) {
      resolved.failed = msg.failed;
    }
    else {
      resolved.failed = false
    }

    return resolved;
    }
};

module.exports = Comunication;
