// Auto-generated. Do not edit!

// (in-package pick_and_delivery.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class OccupiedBool {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.occupied = null;
    }
    else {
      if (initObj.hasOwnProperty('occupied')) {
        this.occupied = initObj.occupied
      }
      else {
        this.occupied = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type OccupiedBool
    // Serialize message field [occupied]
    bufferOffset = _serializer.bool(obj.occupied, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type OccupiedBool
    let len;
    let data = new OccupiedBool(null);
    // Deserialize message field [occupied]
    data.occupied = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    return 1;
  }

  static datatype() {
    // Returns string type for a message object
    return 'pick_and_delivery/OccupiedBool';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '44d7026c7dcc8daf286cc35a49a8f442';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    bool occupied
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new OccupiedBool(null);
    if (msg.occupied !== undefined) {
      resolved.occupied = msg.occupied;
    }
    else {
      resolved.occupied = false
    }

    return resolved;
    }
};

module.exports = OccupiedBool;
