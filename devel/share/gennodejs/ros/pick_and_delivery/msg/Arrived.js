// Auto-generated. Do not edit!

// (in-package pick_and_delivery.msg)


"use strict";

const _serializer = _ros_msg_utils.Serialize;
const _arraySerializer = _serializer.Array;
const _deserializer = _ros_msg_utils.Deserialize;
const _arrayDeserializer = _deserializer.Array;
const _finder = _ros_msg_utils.Find;
const _getByteLength = _ros_msg_utils.getByteLength;

//-----------------------------------------------------------

class Arrived {
  constructor(initObj={}) {
    if (initObj === null) {
      // initObj === null is a special case for deserialization where we don't initialize fields
      this.name = null;
      this.arrived = null;
      this.quit = null;
      this.occupied = null;
    }
    else {
      if (initObj.hasOwnProperty('name')) {
        this.name = initObj.name
      }
      else {
        this.name = '';
      }
      if (initObj.hasOwnProperty('arrived')) {
        this.arrived = initObj.arrived
      }
      else {
        this.arrived = false;
      }
      if (initObj.hasOwnProperty('quit')) {
        this.quit = initObj.quit
      }
      else {
        this.quit = false;
      }
      if (initObj.hasOwnProperty('occupied')) {
        this.occupied = initObj.occupied
      }
      else {
        this.occupied = false;
      }
    }
  }

  static serialize(obj, buffer, bufferOffset) {
    // Serializes a message object of type Arrived
    // Serialize message field [name]
    bufferOffset = _serializer.string(obj.name, buffer, bufferOffset);
    // Serialize message field [arrived]
    bufferOffset = _serializer.bool(obj.arrived, buffer, bufferOffset);
    // Serialize message field [quit]
    bufferOffset = _serializer.bool(obj.quit, buffer, bufferOffset);
    // Serialize message field [occupied]
    bufferOffset = _serializer.bool(obj.occupied, buffer, bufferOffset);
    return bufferOffset;
  }

  static deserialize(buffer, bufferOffset=[0]) {
    //deserializes a message object of type Arrived
    let len;
    let data = new Arrived(null);
    // Deserialize message field [name]
    data.name = _deserializer.string(buffer, bufferOffset);
    // Deserialize message field [arrived]
    data.arrived = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [quit]
    data.quit = _deserializer.bool(buffer, bufferOffset);
    // Deserialize message field [occupied]
    data.occupied = _deserializer.bool(buffer, bufferOffset);
    return data;
  }

  static getMessageSize(object) {
    let length = 0;
    length += object.name.length;
    return length + 7;
  }

  static datatype() {
    // Returns string type for a message object
    return 'pick_and_delivery/Arrived';
  }

  static md5sum() {
    //Returns md5sum for a message object
    return '28548b496855d702abe422d706730c3e';
  }

  static messageDefinition() {
    // Returns full string definition for message
    return `
    string name
    bool arrived
    bool quit
    bool occupied
    
    `;
  }

  static Resolve(msg) {
    // deep-construct a valid message object instance of whatever was passed in
    if (typeof msg !== 'object' || msg === null) {
      msg = {};
    }
    const resolved = new Arrived(null);
    if (msg.name !== undefined) {
      resolved.name = msg.name;
    }
    else {
      resolved.name = ''
    }

    if (msg.arrived !== undefined) {
      resolved.arrived = msg.arrived;
    }
    else {
      resolved.arrived = false
    }

    if (msg.quit !== undefined) {
      resolved.quit = msg.quit;
    }
    else {
      resolved.quit = false
    }

    if (msg.occupied !== undefined) {
      resolved.occupied = msg.occupied;
    }
    else {
      resolved.occupied = false
    }

    return resolved;
    }
};

module.exports = Arrived;
