
"use strict";

let Arrived = require('./Arrived.js');
let Comunication = require('./Comunication.js');
let LoginName = require('./LoginName.js');
let WorkingFor = require('./WorkingFor.js');
let Addressee = require('./Addressee.js');
let Taken = require('./Taken.js');

module.exports = {
  Arrived: Arrived,
  Comunication: Comunication,
  LoginName: LoginName,
  WorkingFor: WorkingFor,
  Addressee: Addressee,
  Taken: Taken,
};
