#include "ros/ros.h"
#include <vector>
#include <sstream>
#include <thread>
#include <chrono>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/PoseWithCovarianceStamped.h"
#include "geometry_msgs/TransformStamped.h"
#include "tf/tf.h"
#include "tf2_ros/transform_listener.h"
#include "tf2_msgs/TFMessage.h"
#include "pick_and_delivery/RoomAddressee.h"
#include "pick_and_delivery/LoginName.h"
#include "pick_and_delivery/Comunication.h"

tf2_ros::Buffer tfBuf;
std::vector<float> addressee_position(2,0);
std::vector<float> old_position(2,0);
std::vector<float> d_comp(2,0);
std::vector<float> actual_position(2,0);
std::vector<float> start_position(2,0);
geometry_msgs::PoseStamped goal_msg;
int cruising = 0;
int publish = 0;
size_t m=0;
int occupied = 0;
std::vector<std::string> rooms;
int room_count = 0;
std::string addr;
std::string send;
pick_and_delivery::Comunication com;
int clientsPublish=0;
int check1 = 0;
int check2 = 0;
int find = 0;

enum state {available, cruiseToSender, sender, cruiseToAddressee, retToSender, addressee};
state s = state::available;

void roomCallback(const pick_and_delivery::LoginName& room){
	if(room.log){
		ROS_INFO("Room logged");
		rooms.push_back(room.name);
		room_count++;
		std::cout<<rooms[room_count-1]<<std::endl;
	}
}

void roomCheckout(std::string unlog_room){
	check1 = 0;
	while(check1<room_count){
		if(rooms[check1]==unlog_room){
			rooms.erase(rooms.begin()+check1);
			room_count--;
			ROS_INFO("Room unlogged");
			break;
		}
		check1++;
	}
	ROS_INFO("Remaining rooms");
	check1 = 0;
	while(check1<room_count){
		std::cout<<rooms[check1]<<std::endl;
		check1++;
	}
	if(!check1) std::cout<<"No rooms"<<std::endl;
}

void returnToBase(){
	goal_msg.header.seq = m;
	m++;
	goal_msg.header.stamp = ros::Time::now();
	goal_msg.header.frame_id = "map";

	goal_msg.pose.position.x = 51.5;
	goal_msg.pose.position.y = 19;
	goal_msg.pose.position.z = 0;

	
	goal_msg.pose.orientation.x = 0;
	goal_msg.pose.orientation.y = 0;
	goal_msg.pose.orientation.z = 0;
	goal_msg.pose.orientation.w = 1;
	publish = 1;
	cruising = 1;

	addressee_position[0] = goal_msg.pose.position.x;
	addressee_position[1] = goal_msg.pose.position.y;
}

void returnToSender(){
	goal_msg.header.seq = m;
	m++;
	goal_msg.header.stamp = ros::Time::now();
	goal_msg.header.frame_id = "map";

	goal_msg.pose.position.x = old_position[0];
	goal_msg.pose.position.y = old_position[1];
	goal_msg.pose.position.z = 0;

	
	goal_msg.pose.orientation.x = 0;
	goal_msg.pose.orientation.y = 0;
	goal_msg.pose.orientation.z = 0;
	goal_msg.pose.orientation.w = 1;
	publish = 1;
	cruising = 1;

	addressee_position[0] = goal_msg.pose.position.x;
	addressee_position[1] = goal_msg.pose.position.y;
}


bool setAddresseeService(pick_and_delivery::RoomAddressee::Request& req, pick_and_delivery::RoomAddressee::Response& res){

	if(s == state::available){
		if(req.room_command == "call"){
			res.response = "sender";
			send = req.room_send;
			s = state::cruiseToSender;
			goal_msg.header.seq = m;
			m++;
			goal_msg.header.stamp = ros::Time::now();
			goal_msg.header.frame_id = "map";
		
			goal_msg.pose.position.x = req.x;
			goal_msg.pose.position.y = req.y;
			goal_msg.pose.position.z = 0;

	
			goal_msg.pose.orientation.x = 0;
			goal_msg.pose.orientation.y = 0;
			goal_msg.pose.orientation.z = 0;
			goal_msg.pose.orientation.w = req.theta;

			old_position[0] = actual_position[0];
			old_position[1] = actual_position[1];

			addressee_position[0] = goal_msg.pose.position.x;
			addressee_position[1] = goal_msg.pose.position.y;
			publish = 1;
			cruising = 1;
		}
		else if(req.room_command == "delete") res.response = "invalid";
		else if(req.room_command == "quit"){
			roomCheckout(req.room_send);
			res.response = "quit";
		}
		else if(req.room_command == req.room_send) res.response = "call";
		
		else res.response = "call";
		return true;
	}


	if(req.room_send != send){
		if(req.room_command == "quit"){
			res.response = "quit";
			roomCheckout(req.room_send);
		}
		else res.response = "busy";
		return true;
	}


	if(s == state::cruiseToSender){
		if(req.room_command == "call") res.response = "wait";
		else if(req.room_command == "delete"){
			returnToBase();
			send = "";
			s = state::available;
			res.response = "delete";
		}
		else if(req.room_command == "quit"){
			returnToBase();
			send = "";
			s = state::available;
			roomCheckout(req.room_send);
			res.response = "quit";
		}
		else res.response = "wait";
		return true;
	}


	if(s == state::sender){
		if(req.room_command == "call") res.response = "invalid";
		else if(req.room_command == "delete"){
			returnToBase();
			send = "";
			s = state::available;
			res.response = "delete";
		}
		else if(req.room_command == "quit"){
			returnToBase();
			send = "";
			s = state::available;
			roomCheckout(req.room_send);
			res.response = "quit";
		}
		else{
			check2 = 0;
			find = 0;
			while(check2 < rooms.size()){
				if (rooms[check2] == req.room_command){
					find = 1;
					break;
				}
				check2++;
			}
			if(find){
				if(req.room_command == send) res.response = "invalid";
				else{
					addr = req.room_addr;
					res.response = "addressee";
					s = state::cruiseToAddressee;
					goal_msg.header.seq = m;
					m++;
					goal_msg.header.stamp = ros::Time::now();
					goal_msg.header.frame_id = "map";
				
					goal_msg.pose.position.x = req.x;
					goal_msg.pose.position.y = req.y;
					goal_msg.pose.position.z = 0;

	
					goal_msg.pose.orientation.x = 0;
					goal_msg.pose.orientation.y = 0;
					goal_msg.pose.orientation.z = 0;
					goal_msg.pose.orientation.w = req.theta;

					old_position[0] = actual_position[0];
					old_position[1] = actual_position[1];
		
					addressee_position[0] = goal_msg.pose.position.x;
					addressee_position[1] = goal_msg.pose.position.y;
					publish = 1;
					cruising = 1;
				}
			}
			else res.response = "inactive";
		}
		return true;
	}


	if(s == state::cruiseToAddressee){
		if(req.room_command == "call") res.response = "invalid";
		else if(req.room_command == "delete"){
			returnToSender();
			addr = "";
			s = state::retToSender;
			res.response = "deleted_addressee";
		}
		else res.response = "invalid";
		return true;
	}


	if(s == state::retToSender){
		res.response = "wait";
		return true;
	}


	if(s == state::addressee){
		res.response = "wait";
		return true;
	}
}






void positionCallback(const tf2_msgs::TFMessage& tf){
	if(tfBuf.canTransform("map", "base_link", ros::Time(0))!=0){
		geometry_msgs::TransformStamped stamp;
		stamp = tfBuf.lookupTransform("map", "base_link", ros::Time(0));
		actual_position[0] = stamp.transform.translation.x;
		actual_position[1] = stamp.transform.translation.y;
	}
}

void stuckCheckCallback(const ros::TimerEvent& event){
	if(cruising){
		ROS_INFO("Stuck check");
		float d_prec = sqrt(pow(actual_position[0]-old_position[0],2) + pow(actual_position[1]-old_position[1],2));
		d_comp[0]=d_comp[1];
		d_comp[1]=d_prec;
		float d_addr = sqrt(pow(actual_position[0]-addressee_position[0],2) + pow(actual_position[1]-addressee_position[1],2));
		std::cout << (d_prec<0.8) << " "<< d_prec << " " << (d_addr>0.5) << std::endl;
		if(d_prec==0) publish = 1;
		if(d_comp[0]==d_comp[1] && d_addr>0.5 && d_prec){
			ROS_INFO("I'm stuck!");
			std::this_thread::sleep_for(std::chrono::seconds(2));
			if(s == state::cruiseToSender){
				com.addressee="stuckInCalling";
				com.sender=send;
				com.arrived=true;
				com.taken=true;
				com.failed=true;
				clientsPublish=1;
				s = state::available;
				send = "";
				returnToBase();
			}
			if(s == state::cruiseToAddressee){
				com.addressee="stuckInSending";
				com.sender=send;
				com.arrived=true;
				com.taken=true;
				com.failed=true;
				clientsPublish=1;
				s = state::retToSender;
				addr = "";
				returnToSender();
			}
			if(s == state::retToSender) returnToSender();
		}
		
		if(d_addr<0.5){
			ROS_INFO("I'm arrived at the addressee");
			cruising = 0;
			if(s == state::cruiseToSender || s == state::retToSender){
				s = state::sender;
				com.addressee="arrivedAtSender";
				com.sender=send;
				com.arrived=true;
				com.taken=true;
				com.failed=true;
				clientsPublish=1;
			}
			if(s == state::cruiseToAddressee){
				com.addressee=addr;
				com.sender=send;
				com.arrived=true;
				com.taken=false;
				com.failed=false;
				clientsPublish = 1;				
			}
		}
	}
}

void clientsCallback(const pick_and_delivery::Comunication res){
	if(res.taken && res.arrived && !res.failed){
		s = state::available;
		returnToBase();
	}
	if(res.failed && !res.taken){
		s = state::retToSender;
		returnToSender();
	}
}
		


int main(int argc, char **argv){
	ros::init(argc, argv, "Server_PickAndDelivery");
	ROS_INFO("Hello, i'm ready to start");
	ros::NodeHandle n;
	ros::Subscriber room_sub = n.subscribe("server", 1000, roomCallback);
	ros::ServiceServer set_adr = n.advertiseService("addressee", setAddresseeService);
	ros::Publisher goal_pub = n.advertise<geometry_msgs::PoseStamped>("/move_base_simple/goal", 1000);
	ros::Publisher clients_pub = n.advertise<pick_and_delivery::Comunication>("clients", 1000);
	ros::Subscriber clients_sub = n.subscribe("clients", 1000, clientsCallback);


	tf2_ros::TransformListener tfList(tfBuf);

	ros::Rate loop_rate(100);
	
	ros::Subscriber sub_tf = n.subscribe("tf", 1000, positionCallback);


	ros::Timer stuck_t = n.createTimer(ros::Duration(0.5), stuckCheckCallback);

	int count=0;
	while(ros::ok()){
		if(clientsPublish){
			clientsPublish=0;
			clients_pub.publish(com);
		}
		if(publish){
			ROS_INFO("New addressee received");
			goal_pub.publish(goal_msg);
			publish=0;
		}
		ros::spinOnce();
		loop_rate.sleep();
		count++;
	}
}
