#include "ros/ros.h"
#include <cstdlib>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <cstdio>
#include <thread>
#include <chrono>
#include "pick_and_delivery/RoomAddressee.h"
#include "pick_and_delivery/LoginName.h"
#include "pick_and_delivery/Comunication.h"

struct Room{
	std::string name;
	float x;
	float y;
	float theta;
	std::string id_code;
};
int command = 0;
std::string addressee;
Room login_room;
pick_and_delivery::Comunication end_del;
int end_publish = 0;
int occupied = 0;
int srv_publish = 0;
int comparison = 0;
int publish = 0;
int reminder = 0;
int called = 0;
int cruising = 0;
int stuck = 0;
std::string sender_room;
std::string addressee_room;
std::vector<Room> rooms;
pick_and_delivery::RoomAddressee room_cor;


void commandInsert(){
	std::string addr;
	if(!publish) std::cout << "Insert command" << std::endl;
	std::cin >> addr;
	if(!occupied){
		comparison = 1;
		addressee=addr;
		end_publish = 1;
		end_del.sender=login_room.name;
		if(called && !cruising && !stuck) end_del.addressee=addr;
		end_del.taken=true;
		end_del.arrived=true;
		end_del.failed=true;
	}
	if(addr=="taken" && occupied){
		end_publish = 1;
		end_del.sender=sender_room;
		end_del.addressee=login_room.name;
		end_del.taken=true;
		end_del.arrived=true;
		end_del.failed=false;
		std::system("clear");
		std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
		std::cout<<"Thank you, bye bye\n"<<std::endl;
		occupied = 0;
		command = 0;
		reminder = 0;
	}
}

void timerTaken(){
	std::system("clear");
	std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
	std::cout << "A robot is here for you!\nTake the object and type 'taken'" << std::endl;
	std::this_thread::sleep_for(std::chrono::seconds(10));
	if(occupied){
		std::cout << "\nToo late, sorry. Goodbye\n" << std::endl;
		end_del.sender=sender_room;
		end_del.addressee=login_room.name;
		end_del.taken=false;
		end_del.arrived=true;
		end_del.failed=true;
		end_publish = 1;
		occupied = 0;
		command = 0;
		reminder = 0;
	}
}

int Room_load(Room* r, const char* filename) {
	FILE* f;
	f=std::fopen(filename, "r");
	if (!f) {
		std::cout << "Error in file opening" << std::endl;
		return 0;
	}
	char* buffer = NULL;
	size_t line_length = 0;
	int tokens;
	int insert = 0;
	char name[10];
	char id_code[10];
	float x, y, theta;
	while(getline(&buffer, &line_length, f)>0){
		tokens=sscanf(buffer, "Room name %s", name);
		if (tokens==1){
			r->name=name;
			std::cout <<"NAME: "<< r->name << std::endl;
			++insert;
			continue;
		}
		tokens=sscanf(buffer, "X %f", &x);
		if (tokens==1){
			r->x=x;
			std::cout << "X: " << r->x << std::endl;
			++insert;
			continue;
		}
		tokens=sscanf(buffer, "Y %f", &y);
		if (tokens==1){
			r->y=y;
			std::cout << "Y: " << r->y << std::endl;
			++insert;
			continue;
		}
		tokens=sscanf(buffer, "Theta %f", &theta);
		if (tokens==1){
			r->theta=theta;
			std::cout << "THETA: " << r->theta << std::endl;
			++insert;
			continue;
		}
		tokens=sscanf(buffer, "Room ID code %s", id_code);
		if (tokens==1){
			r->id_code=id_code;
			std::cout << "ID CODE: " << r->id_code << std::endl;
			++insert;
			continue;
		}
	}
	if (buffer) free(buffer);
	fclose(f);
	return insert;
}

void commandCall(){
	room_cor.request.x=login_room.x;
	room_cor.request.y=login_room.y;
	room_cor.request.theta=login_room.theta;
	room_cor.request.room_addr="sender room";
	room_cor.request.room_command="call";
	room_cor.request.room_send=login_room.name;
	srv_publish = 1;
	called = 1;
}

void commandDelete(){
	room_cor.request.room_command = "delete";
	srv_publish = 1;
}

void commandRoom(int room_num){
	if(!called){
		system("clear");
		std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
		std::cout<<"Call the robot first"<<std::endl;
		if(reminder){
			std::cout << "\nSomething is coming for you...\n" << std::endl;
		}
	}
	if(called && rooms[room_num].name!=login_room.name){
		room_cor.request.x = rooms[room_num].x;
		room_cor.request.y = rooms[room_num].y;
		room_cor.request.theta = rooms[room_num].theta;
		room_cor.request.room_addr = rooms[room_num].name;
		room_cor.request.room_send = login_room.name;
		room_cor.request.room_command = rooms[room_num].name;
		srv_publish = 1;
		addressee_room = room_cor.request.room_addr;
	}
}

void commandQuit(){
	if(reminder){
		std::system("clear");
		std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
		std::cout<<"You can't quit, please, wait"<<std::endl;
		if(reminder){
			std::cout << "\nSomething is coming for you...\n" << std::endl;
		}
	} else {
		room_cor.request.room_command = "quit";
		room_cor.request.room_send = login_room.name;
		srv_publish = 1;
	}
}

void commandCommands(){
	std::cout<<"COMMANDS LIST:"<<std::endl;
	std::cout<<"\ncall:\ncall the robot to you"<<std::endl;
	std::cout<<"\ndelete:\nannull the last command/free the robot"<<std::endl;
	std::cout<<"\nquit:\nunlogg you from system"<<std::endl;
	std::cout<<"\nroom_name:\nchoose the addressee (after a call)"<<std::endl;
	std::cout<<"\ncommands:\nspawn this commands list\n"<<std::endl;
	command = 0;
}

void clientsCallback(const pick_and_delivery::Comunication& comm){
	if(login_room.name==comm.sender && comm.addressee!="request deleted"){
		if(comm.arrived && comm.taken && comm.failed){
			if(comm.addressee=="stuckInCalling"){
				system("clear");
				std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
				std::cout<<"Sorry, but i had a problem. I go back to the base"<<std::endl;
				command = 0;
				publish = 0;
				called = 0;
				cruising = 0;
			}
			else if(comm.addressee=="stuckInSending"){
				system("clear");
				std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
				std::cout<<"Sorry, but i had a problem. I come back to you"<<std::endl;
				end_del.addressee=addressee_room;
				end_del.sender="stuckInSending";
				end_del.arrived=true;
				end_del.taken=true;
				end_del.failed=true;
				end_publish=1;
				addressee_room = "";
				stuck = 1;
			}
			else if(comm.addressee=="arrivedAtSender"){
				std::system("clear");
				std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
				std::cout<<"The robot is here"<<std::endl;
				occupied = 0;
				publish = 0;
				command = 0;
				cruising = 0;
				stuck = 0;
			}
		}
		if(comm.arrived && !comm.taken){
			system("clear");
			std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
			occupied = 1;
			cruising = 0;
			std::cout << "Robot arrived, waiting for the addressee\n" << std::endl;
		}
		if(comm.failed && !comm.taken){
			std::cout << "Package not taken, robot is coming back\n" << std::endl;
			command = 0;
			occupied = 0;
			stuck = 1;
			addressee_room = "";
		}
		if(comm.taken && !comm.failed){
			std::cout << "Package sent, bye bye\n" << std::endl;
			publish = 0;
			command = 0;
			occupied = 0;
			addressee_room = "";
			called = 0;
		}
		if(comm.addressee==""){
			command = 0;
		}
	}
	if(login_room.name==comm.addressee){
		if(comm.arrived && comm.taken && comm.failed && (comm.sender=="stuckInSending" || comm.sender=="delete")){
			std::cout<<"I lied, sorry, maybe late :(\n"<<std::endl;
			std::cout<<"Insert command"<<std::endl;
			reminder = 0;
		}
		if(comm.arrived && comm.taken && comm.failed && comm.sender !="stuckInSending" && comm.sender!="delete" && comm.sender!=comm.addressee){
			sender_room=comm.sender;
			std::cout << "\nSomething is coming for you...\n" << std::endl;
			reminder = 1;
		}
		if(comm.arrived && !comm.taken && !comm.failed){
			occupied = 1;
			std::string t="";
			std::thread timer(timerTaken);
			timer.join();
		}
	}
}

int main(int argc, char **argv){
	ros::init(argc, argv, "Client_PickAndDelivery");
	ros::NodeHandle n;
	ros::Publisher server_pub = n.advertise<pick_and_delivery::LoginName>("server", 1000);
	ros::Subscriber clients_sub = n.subscribe("clients", 1000, clientsCallback);
	ros::Publisher clients_pub = n.advertise<pick_and_delivery::Comunication>("clients", 1000);
	if (argc < 2){
		ROS_INFO("Insert available rooms\n");
		return 1;
	}
	std::cout<<"\t---ENTERING AVILABLE ROOMS---\n";
	for (int i=1; i<argc; ++i){
		Room new_room;
		std::cout << "\n*-Entering room-*\n" << std::endl;
		int success = Room_load(&new_room, argv[i]);
		if (success!=5){
			std::cout << "Error in passed file" << std::endl;
		}
		std::cout << "ADDED: " << new_room.name << std::endl;
		rooms.push_back(new_room);
	}
	if(rooms.size()<0){
		std::cout << "\tNO ADDED ROOMS" << std::endl;
		return 1;
	}
	ros::Rate loop_rate(100);
	ros::ServiceClient adr_srv = n.serviceClient<pick_and_delivery::RoomAddressee>("addressee");
	std::string room_name, room_code;
	int find = 0;
	while(!find){
		std::cout << "\nInsert ROOM NAME:" << std::endl;
		std::cin >> room_name;
		if(room_name=="q"){
			ros::shutdown();
			find=1;
		}
		std::cout << "\nInsert ROOM ID CODE:" << std::endl;
		std::cin >> room_code;
		if(room_code=="q"){
			ros::shutdown();
			find=1;
		}
		if(find) return 1;
		for (int k=0; k<rooms.size(); k++){
			if (room_name==rooms[k].name && room_code==rooms[k].id_code){
				login_room.x=rooms[k].x;
				login_room.y=rooms[k].y;
				login_room.theta=rooms[k].theta;
				login_room.name=rooms[k].name;
				login_room.id_code=rooms[k].id_code;
				find = 1;

			}
		}
		if(!find) std::cout<<"\n\tINVALID LOGIN\n"<<std::endl;
	}
	
	pick_and_delivery::LoginName ln;
	ln.name = login_room.name;
	ln.log = true;
	server_pub.publish(ln);
				
	std::system("clear");
	std::cout<<"\t---THE CLIENT IS NOW ACTIVE---\n"<<std::endl;
	std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
	commandCommands();
	while (ros::ok()){
		if(!command){
			std::thread insert(commandInsert);
			insert.detach();
			command = 1;
		}
		
		if(end_publish){
			end_publish = 0;
			clients_pub.publish(end_del);
			end_del.addressee = "";
		}

		if(comparison){
			find = 0;
			for (int j=0; j<rooms.size(); j++){
				if (addressee==rooms[j].name && rooms[j].name!=login_room.name){
					commandRoom(j);
					find = 1;
					break;
				}
			}
			if (addressee=="quit"){
				commandQuit();
				find = 1;
			}
			if (addressee=="delete"){
				commandDelete();
				find = 1;
			}
			if (addressee=="call"){
				commandCall();
				find = 1;
			}
			if (addressee=="commands"){
				system("clear");
				std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
				commandCommands();
				find = 1;
			}
			if (!find){
				std::cout << "\nInvalid command\n" << std::endl;
				if(reminder){
					std::cout << "\nSomething is coming for you...\n" << std::endl;
				}
			}
			comparison = 0;
			command = 0;
		}

		if(srv_publish){
			srv_publish = 0;
			if(!adr_srv.call(room_cor)){
				ROS_INFO("THERE'S SOMETHING WRONG (maybe the server is offline)");
				if(addressee=="quit") ros::shutdown();
			}
			else{
				if(room_cor.response.response == "wait"){
					std::cout<<"\nWait for the end of the request"<<std::endl;
					command = 0;
				}
				else if(room_cor.response.response == "delete"){
					std::system("clear");
					std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
					std::cout<< "Request deleted\n" << std::endl;
					command = 0;
					publish = 0;
					called = 0;
				}
				else if(room_cor.response.response == "invalid"){
					std::cout<< "\nInvalid request\n"<<std::endl;
					command = 0;
				}
				else if(room_cor.response.response == "quit"){
					std::system("clear");
					std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
					std::cout<<"Unlogged, bye bye!"<<std::endl;
					ros::shutdown();
				}
				else if(room_cor.response.response == "busy"){
					std::system("clear");
					std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
					std::cout<<"Sorry, the robot is already busy. Try later"<<std::endl;
					command = 0;
					publish = 0;
					called = 0;
				}
				else if(room_cor.response.response == "sender"){
					std::system("clear");
					std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
					std::cout<<"Coming to sender"<<std::endl;
					cruising = 1;
					command = 0;
					publish = 1;
				}
				else if(room_cor.response.response == "addressee"){
					std::system("clear");
					std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
					std::cout<<"Starting cruise to "<<addressee<<std::endl;
					command = 0;
					publish = 1;
					cruising = 1;
				}
				else if(room_cor.response.response == "call"){
					std::system("clear");
					std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
					std::cout<<"Use command 'call' to call the robot\n"<<std::endl;
					command = 0;
					publish = 0;
				}
				else if(room_cor.response.response == "deleted_addressee"){
					std::system("clear");
					std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
					std::cout<<"Addressee deleted, come back to sender\n"<<std::endl;
					command = 0;
					publish = 1;
					cruising = 1;
					end_del.addressee = addressee_room;
					end_del.sender = "delete";
					end_publish = 1;
					addressee_room = "";
				}
				else if(room_cor.response.response == "inactive"){
					std::system("clear");
					std::cout<<"\t---ROOM "<<login_room.name<<"---\n"<<std::endl;
					std::cout<<"Request for inactive room, choose again\n"<<std::endl;
					command = 0;
					publish = 0;
				}
				if(reminder){
					std::cout << "\nSomething is coming for you...\n" << std::endl;
				}
			}
		}


		ros::spinOnce();
		loop_rate.sleep();
	}
}



















